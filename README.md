# HPC-connector

### Simulating the execution of a dummy job with logs:

![Job example](Job_simulate_with_logs.JPG)

### Help commands

```bash
[serlophug.DESKTOP-0133E8H] ➤ python hpc-connector.py -h

optional arguments:
  -h, --help            show this help message and exit
  --backend {Prometheus}
                        Select the backend to run the job
  --backend-conf BACKEND_CONF
                        Dictionary with specific backend configuration. Use
                        the help subcommand to see the required variables.
                        Example: "{}"

Subcommands:
  {job,simulate,file,directory,help}
                        Valid subcommands
    job                 Executes the option selected iteracting with the
                        backend
    simulate            Submits a job in the selected backend and waits until
                        it ends
    file                Executes the option selected iteracting with the
                        backend
    directory           Executes the option selected iteracting with the
                        backend
    help                Executes the help of selected backend
```
```bash
[serlophug.DESKTOP-0133E8H] ➤ python hpc-connector.py job -h
usage: hpc-connector.py job [-h] [--id ID] [--job-config JOB_CONFIG]
                            [--monitoring-period MONITORING_PERIOD]
                            {submit,delete,cancel,info,monitoring}

positional arguments:
  {submit,delete,cancel,info,monitoring}
                        Option to execute

optional arguments:
  -h, --help            show this help message and exit
  --id ID               Job name or ID
  --job-config JOB_CONFIG
                        Dictionary with specific job configuration for the
                        selected backend. Use the help subcommand to see the
                        required variables
  --monitoring-period MONITORING_PERIOD
                        Seconds between getting information about the job
                        state in the backend. Default: 300 seconds

```
```bash
[serlophug.DESKTOP-0133E8H] ➤ python hpc-connector.py simulate -h
usage: hpc-connector.py simulate [-h] [--job-config JOB_CONFIG]
                                 [--monitoring-period MONITORING_PERIOD]
                                 [--print-logs] [--logs-config LOGS_CONFIG]

optional arguments:
  -h, --help            show this help message and exit
  --job-config JOB_CONFIG
                        Dictionary with specific job configuration for the
                        selected backend. Use the help subcommand to see the
                        required variables
  --monitoring-period MONITORING_PERIOD
                        Seconds between getting information about the job
                        state in the backend. Default: 300 seconds
  --print-logs          Print logs at the end of the execution. If additional
                        configuration is required of your backend for gatter
                        the logs, use also the argument "--logs-config"
  --logs-config LOGS_CONFIG
                        Dictionary with specific logs configuration for the
                        selected backend. Use the help subcommand to see the
                        required variables

```
```bash
[serlophug.DESKTOP-0133E8H] ➤ python hpc-connector.py file -h
usage: hpc-connector.py file [-h] [--dest DEST] {upload,download,remove} src

positional arguments:
  {upload,download,remove}
                        Option to execute
  src                   It is the file that will be uploaded to the backend or
                        the URI at the backend that will be dowloaded or
                        removed

optional arguments:
  -h, --help            show this help message and exit
  --dest DEST           Destination URI in the backend. If it is used for the
                        command "download", the file will be stored in that
                        path.
```
```bash
[serlophug.DESKTOP-0133E8H] ➤ python hpc-connector.py directroy -h
usage: hpc-connector.py directory [-h] {list,create,remove} path

positional arguments:
  {list,create,remove}  Option to execute
  path                  Directory URI in the backend to be listed, created o
                        removed

optional arguments:
  -h, --help            show this help message and exit
```

```bash
[serlophug.DESKTOP-0133E8H] ➤ python hpc-connector.py --backend Prometheus --backend-conf "{}" help

Showing BACKEND Prometheus help:
--backend-conf dictionary:
        - ENDPOINT: Complete host url. Example: https://submit.plgrid.pl
        - PROXY: Value of PROXY header

JOB --job-config dictionary:
        - script: String with the SLURM script
        - host: String with host. Example: prometheus.cyfronet.pl
        - working_directory (optional): By default working_directory is set to user home directory
        - tag (optional)

SIMULATE --logs-config dictionary: Not required, logs file path are obtained in the job information
```
