FROM alpine
MAINTAINER serlohu@upv.es
LABEL version = '0.0.1'

RUN apk add --no-cache python3 && apk add py-pip && apk add bash && apk add bash-doc && apk add bash-completion && pip3 install --no-cache-dir --upgrade pip && pip3 install setuptools

RUN mkdir /opt/hpc-connector
COPY src/ /opt/hpc-connector/src
COPY setup.py /opt/hpc-connector/
COPY hpc-connector.py /opt/hpc-connector/
COPY requirements.txt /opt/hpc-connector/

RUN cd /opt/hpc-connector/ && python3 setup.py install
ENTRYPOINT ["/usr/bin/hpc-connector.py"]